import { useJsMethods } from '@/stores/jsStore'

const jsStore = useJsMethods()

function callScriptContent(fnName, val, method) {
  var firstLine = ''

  if (method == 'array') {
    firstLine = `const array1 = ${JSON.stringify(val)}`
  } else if (method == 'object') {
    firstLine = `const target = ${JSON.stringify(val)}`
  }
  return jsFn(firstLine)[fnName]()
}
const jsFn = (firstLine) => {
  const scriptData = {}
  jsStore.jsScripts.forEach((element) => {
    scriptData[element.id] = () => `${firstLine}
${element.desc}`
  })
  return scriptData
}
export { callScriptContent }
