import { ref } from 'vue'
const isDark = ref(localStorage.getItem('isDark') == 'false' ? false : true)

const updateTheme = (val) => {
  localStorage.setItem('isDark', `${val}`)
  isDark.value = val
}
const allProviders = {
  isDark: {
    isDark,
    updateTheme
  }
}
const provideSharedData = (app) => {
  for (const [key, value] of Object.entries(allProviders)) {
    app.provide(key, value)
  }
}
const useCopyToClipboard = async (text) => {
  const status = await navigator.clipboard
    .writeText(text)
    .then(() => {
      return { msg: 'Link copied', color: 'is-success', success: true }
    })
    .catch(() => {
      return { msg: 'Sorry ! Error', color: 'is-danger', success: false }
    })
  return status
}

export { provideSharedData, useCopyToClipboard }
