// stores/counter.js
import { defineStore } from 'pinia'
import { db } from '../firebase'
import { doc, getDoc, collection, writeBatch } from 'firebase/firestore'

import { useCollection } from 'vuefire'
import { computed, ref } from 'vue'
import { useRoute } from 'vue-router'

export const useJsMethods = defineStore('jsMethods', () => {
  const route = useRoute()
  const params = route?.params.id ?? []
  const method = params[0] ?? 'array'

  const prototypes = useCollection(collection(db, 'javascript', method, 'prototypes'))
  const jsScripts = useCollection(collection(db, 'javascript', method, 'script'))
  const jsRoutes = ref([])

  const jsInput = ref([])

  async function inputData() {
    const docRef = doc(db, 'javascript', method)
    const { data } = await getDoc(docRef).then((data) => {
      if (data.exists()) {
        return {
          success: true,
          data: data.data().input
        }
      }
      return {
        success: false,
        data: {}
      }
    })
    jsInput.value = data
  }

  async function jsConceptsDetails() {
    const docRef = doc(db, 'javascript', 'concepts')
    const { data } = await getDoc(docRef).then((data) => {
      if (data.exists()) {
        return {
          success: true,
          data: data.data().id
        }
      }
      return {
        success: false,
        data: {}
      }
    })
    data.forEach(async (element) => {
      const docRef = doc(db, 'routes', element)
      await getDoc(docRef).then((data) => {
        if (data.exists()) {
          jsRoutes.value.push(data.data())
        }
      })
    })
  }

  async function updateScripts(prototypesUp, method) {
    const batch = writeBatch(db)
    prototypesUp.forEach((item) => {
      const updateScript = doc(db, 'javascript', method, 'script', item.key)
      const updatePrototype = doc(db, 'javascript', method, 'prototypes', item.key)
      batch.set(updatePrototype, {
        key: item.key,
        desc: item.desc,
        value: item.value,
        syntax: item.syntax.split('||')
      })
      batch.set(updateScript, { desc: item.script })
    })
    await batch.commit()
  }

  function getCollectionDetails(method) {
    const prototypes = useCollection(collection(db, 'javascript', method, 'prototypes'))
    const jsScripts = useCollection(collection(db, 'javascript', method, 'script'))
    return {
      prototypes,
      jsScripts
    }
  }

  const getListItems = computed(() => prototypes.value)
  function addjsInput(val) {
    if (method == 'array') {
      jsInput.value.push(val)
    } else if (method == 'object') {
      jsInput.value[val.key] = val.value
    }
  }
  function deletejsInput(index) {
    if (method == 'array') {
      jsInput.value.splice(index, 1)
    } else if (method == 'object') {
      delete jsInput.value[index]
    }
  }

  return {
    prototypes,
    jsInput,
    jsRoutes,
    getListItems,
    jsScripts,
    addjsInput,
    deletejsInput,
    inputData,
    jsConceptsDetails,
    updateScripts,
    getCollectionDetails
  }
})
