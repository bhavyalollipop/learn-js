import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
// ... other firebase imports
const firebaseConfig = {
  apiKey: 'AIzaSyCAjnAvDJQMrQ9LyR5zSsXrAyglLj3i8ls',
  authDomain: 'learn-js-b2fd7.firebaseapp.com',
  projectId: 'learn-js-b2fd7',
  storageBucket: 'learn-js-b2fd7.appspot.com',
  messagingSenderId: '185449460810',
  appId: '1:185449460810:web:0841f74b6406e0a65f22b3'
}
const firebaseApp = initializeApp(firebaseConfig)

// used for the firestore refs
const db = getFirestore(firebaseApp)

export { firebaseApp, db }
