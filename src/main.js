import { createApp } from 'vue'
import { createPinia } from 'pinia'
import 'bulma/css/bulma.css'
import './assets/main.css'
import App from './App.vue'
import router from './router'
import { VueFire, VueFireAuth } from 'vuefire'
import SearchVue from './views/Search.vue'
import { provideSharedData } from './helpers/useCommon.js'
import { firebaseApp } from './firebase'
import '@vuepic/vue-datepicker/dist/main.css'
const app = createApp(App)

app.use(VueFire, {
  // imported above but could also just be created here
  firebaseApp,
  modules: [
    // we will see other modules later on
    VueFireAuth()
  ]
})
app.use(createPinia()).use(router).use(provideSharedData)
app.component('Search', SearchVue)
app.mount('#app')
