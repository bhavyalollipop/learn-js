import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import Admin from '@/views/Admin.vue'
import NotFound from '@/components/errorComponent.vue'
const router = createRouter({
  history: createWebHistory(),
  base: '/',
  routes: [
    {
      path: '/:id*',
      component: Home,
      meta: {
        show: true,
        title: 'Array Methods',
        desc: 'List of all Array methods in javascript'
      }
    },
    {
      path: '/admin',
      component: Admin
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: NotFound,
      meta: {}
    }
  ]
})

export default router
